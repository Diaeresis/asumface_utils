module utils;

import std.traits;

/** Consumes one T from the range and returns it. */
T consume(T, R)(ref R arr)
{
    import std.array;
    static assert(is(ElementType!R : const(ubyte)));
    ubyte[T.sizeof] buf;
    foreach (i; 0 .. T.sizeof)
    {
        buf[i] = arr.front;
        arr.popFront;
    }
    return *cast(T*)&buf[0];
}

auto swap_be(T)(T _val) pure nothrow @safe @nogc
    if (isIntegral!T || isFloatingPoint!T)
{
    static if (isFloatingPoint!T)
        auto val = as_integral(_val);
    else
        Unqual!T val = _val;

    version (BigEndian)
        return val;
    ubyte[T.sizeof] bytes;
    foreach (ref e; bytes[])
    {
        e = cast(ubyte)(val & 0xff);
        val >>>= 8;
    }
    val = 0;
    foreach (e; bytes[])
    {
        val <<= 8;
        val |= e;
    }
    static if (isFloatingPoint!T)
        return cast(T)as_float(val);
    else
        return cast(T)val;
}

private ulong as_integral(T : double)(ref T val) pure nothrow @trusted @nogc
{
    return *cast(ulong*)&val;
}

private uint as_integral(T : float)(ref T val) pure nothrow @trusted @nogc
{
    return *cast(uint*)&val;
}

private float as_float(T : uint)(ref T val) pure nothrow @trusted @nogc
{
    return *cast(float*)&val;
}

private double as_float(T : ulong)(ref T val) pure nothrow @trusted @nogc
{
    return *cast(double*)&val;
}

T[] as_array(T)(ref T v) pure nothrow @trusted @nogc
{
    return (&v)[0 .. 1];
}

int cmp_vecs(T)(T lhs, T rhs)
{
    foreach (i; 0 .. T.v.length)
    {
        if (lhs.v[i] < rhs.v[i])
            return -1;
        if (lhs.v[i] > rhs.v[i])
            return 1;
    }
    return 0;
}

E[] dedup(bool keepnone, E)(E[] input)
{
    import std.array;
    import std.algorithm : move;
    if (input.length < 2)
        return input;
    auto output = input;
    size_t wcur = 0;

    while (!input.empty)
    {
        auto register = input.front;
        input.popFront;

        size_t count_dups()
        {
            auto result = 0;
            auto input = input;
            while (!input.empty && input.front == register)
            {
                input.popFront;
                result += 1;
            }
            return result;
        }

        auto dups = count_dups();
        input = input[count_dups .. $];
        if (dups == 0 || dups > 0 && !keepnone)
            output[wcur++] = register;
    }
    return output[0 .. wcur];
}