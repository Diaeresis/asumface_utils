module cache;

private struct metadata_t
{
    @safe:
    @nogc:
    pure:
    nothrow:

    ubyte _store;

    bool referenced()
    {
        return (_store & 1 << 1) == true;
    }

    bool referenced(bool status)
    {
        return ((_store & ~(1 << 1)) | status) == true;
    }
}

struct clock_cache_t(K, V, int size)
{
    metadata_t[size] metadata;
    K[size] key_store;
    V[size] value_store;
    int hand;
    size_t[3] counters;

    V query(F, KF)(scope F miss_fun, scope KF keyfun, K key)
    {
        foreach (i, ref k; key_store)
        {
            if (key == k) // cache hit
            {
                counters[0] += 1;
                metadata[i].referenced = true;
                return value_store[i];
            }
        }
        // cache miss
        while (metadata[hand].referenced == true)
        {
            metadata[hand].referenced = false;
            hand += 1;
            hand %= size;
        }

        assert(metadata[hand].referenced == false);
        //destroy(value_store[hand]);        stderr.writefln!"set %s"(value_store[hand]);

        key_store[hand] = null;
        value_store[hand] = miss_fun(key);
        counters[1] += 1;
        key_store[hand] = keyfun(value_store[hand], key);
        metadata[hand].referenced = true;
        int orig_hand = hand;
        hand += 1;
        hand %= size;
        return value_store[orig_hand];
    }

    V nested_query(F)(F miss_fun, K key)
    {
        foreach (i, ref k; key_store)
        {
            if (key == k) // cache hit
            {
                counters[0] += 1;
                metadata[i].referenced = true;
                return value_store[i];
            }
        }
        // cache miss, do not update structure
        counters[2] += 1;
        return miss_fun(key);
    }

    bool evict(K key)
    {
        foreach (i, ref k; key_store)
        {
            if (key == k)
            {
                metadata[i].referenced = false;
                k = null;
                //destroy(value_store[i]);
                return true;
            }
        }
        return false;
    }

    void evict_all()
    {
        foreach (i; 0 .. size)
        {
            metadata[i].referenced = false;
            key_store[i] = null;
            //destroy(value_store[i]);
        }
    }
}
